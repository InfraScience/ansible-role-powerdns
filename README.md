Role Name
=========

A role that installs a PowerDNS authoritative service, <https://www.powerdns.com/powerdns-authoritative-server>

Role Variables
--------------

The most important variables are listed below:

``` yaml
powerdns_auth_major: 4
powerdns_auth_minor: 9
# From https://repo.powerdns.com
powerdns_auth_version: '{{ powerdns_auth_major }}.{{ powerdns_auth_minor }}'
powerdns_auth_repo_data: 'deb [arch=amd64] http://repo.powerdns.com/ubuntu {{ ansible_distribution_release }}-auth-{{ powerdns_auth_major }}{{ powerdns_auth_minor }} main'
powerdns_auth_repo_key: 'https://repo.powerdns.com/FD380FBB-pub.asc'

powerdns_auth_enable_api: true
powerdns_auth_enable_web_interface: true

powerdns_auth_pkgs:
  - pdns-server
  - pdns-tools
  - pdns-backend-lua
  - pdns-backend-remote 
  - pdns-backend-pipe 

powerdns_auth_use_db_backend: true
powerdns_auth_backend_name: gpgsql
powerdns_auth_db_backend: pgsql
powerdns_auth_db_pkgs:
  - 'pdns-backend-{{ powerdns_auth_db_backend }}'

powerdns_auth_backend_data:
  - { key: 'gpgsql-host', value: '127.0.0.1' }
  - { key: 'gpgsql-port', value: 5432 }
  - { key: 'gpgsql-dbname', value: '' }
  - { key: 'gpgsql-user', value: '' }
  - { key: 'gpgsql-password', value: '' }
  - { key: 'gpgsql-dnssec', value: '' }
  - { key: 'gpgsql-extra-connection-parameters', value: '' }
```

Dependencies
------------

None

License
-------

EUPL-1.2

Author Information
------------------

Andrea Dell'Amico, <andrea.dellamico@isti.cnr.it>
