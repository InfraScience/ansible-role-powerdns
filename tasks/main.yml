---
- name: Manage the PowerDNS repository on Deb systems
  tags: ['dns', 'powerdns', 'powerdns_repository']
  when: ansible_distribution == "Ubuntu"
  block:
    - name: Stop and disable systemd-resolved
      ansible.builtin.service:
        name: systemd-resolved
        state: stopped
        enabled: false
      when: ansible_distribution_major_version >= '18'
    - name: Ensure that /etc/apt.d/keyrings exists
      ansible.builtin.file:
        dest: /etc/apt/keyrings
        state: directory
        owner: root
        group: root
        mode: "0755"
    - name: Get the signing key for the powerdns.com repository
      ansible.builtin.get_url:
        url: "{{ powerdns_auth_key_url }}"
        dest: "{{ powerdns_auth_repo_key }}"
        owner: root
        group: root
        mode: "0644"
    - name: Install the repository for Ubuntu
      deb822_repository:
        name: powerdns-com
        types: [deb]
        uris: "{{ powerdns_auth_repo_url }}"
        components:
          - main
        suites: ["{{ powerdns_auth_repo_rel }}"]
        signed_by: "{{ powerdns_auth_repo_key }}"
        state: present
        enabled: true
      notify: Update the apt cache
    - name: PowerDNS repo update flush handlers
      ansible.builtin.meta: flush_handlers
- name: Manage the PowerDNS packages on deb systems
  tags: ['dns', 'powerdns', 'powerdns_packages']
  when: ansible_distribution == "Ubuntu"
  block:
    - name: Install the powerdns packages
      ansible.builtin.apt:
        pkg: "{{ powerdns_auth_pkgs }}"
        state: present
        cache_valid_time: 1800
    - name: Install the powerdns DB backend packages
      ansible.builtin.apt:
        pkg: "{{ powerdns_auth_db_pkgs }}"
        state: present
        cache_valid_time: 1800
      when: powerdns_auth_use_db_backend

- name: Manage the PowerDNS repository and packages on EL systems
  tags: ['dns', 'powerdns', 'powerdns_repository', 'powerdns_packages']
  when: ansible_distribution_file_variety == "RedHat"
  block:
    - name: Get the powerdns repository file
      ansible.builtin.get_url:
        url: "{{ powerdns_auth_el_repo }}"
        dest: "/etc/yum.repos.d/powerdns-auth-{{ powerdns_auth_major }}{{ powerdns_auth_minor }}.repo"
        owner: root
        group: root
        mode: "0444"
    - name: Install the powerdns packages on EL systems
      ansible.builtin.dnf:
        pkg: "{{ powerdns_auth_el_packages }}"
        state: present
    - name: Install the powerdns DB backend packages
      ansible.builtin.dnf:
        pkg: "{{ powerdns_auth_db_pkgs }}"
        state: present
      when: powerdns_auth_use_db_backend

- name: Install pgsql db schema
  tags: ['dns', 'powerdns', 'db_schema_conf']
  when: powerdns_auth_use_db_backend
  become_user: postgres
  ansible.builtin.command: >
    psql -U {{ powerdns_db_user }} -d {{ powerdns_db_name }} -f {{ powerdns_schema_path_file }}
  register: init_db_schema
  changed_when: "'CREATE TABLE' in init_db_schema.stdout"

- name: Manage the PowerDNS configuration
  tags: ['dns', 'powerdns', 'powerdns_conf']
  block:
    - name: Install the powerdns main configuration file
      ansible.builtin.template:
        src: pdns.conf.j2
        dest: /etc/powerdns/pdns.conf
        owner: root
        group: pdns
        mode: "0640"
      notify: Restart powerdns
    - name: Install the powerdns local configuration file
      ansible.builtin.template:
        src: pdns.local.conf.j2
        dest: /etc/powerdns/pdns.d/pdns.local.conf
        owner: root
        group: pdns
        mode: "0640"
      notify: Restart powerdns
 


- name: Manage the PowerDNS service
  tags: ['dns', 'powerdns', 'powerdns_service']
  block:
    - name: Ensure that powerdns is running and enabled
      ansible.builtin.service:
        name: pdns
        state: started
        enabled: true
